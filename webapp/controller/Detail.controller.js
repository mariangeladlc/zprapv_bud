sap.ui.define(
  [
    "./BaseController",
    "sap/ui/model/json/JSONModel",
    "../model/formatter",
    "sap/m/library",
    "sap/m/MessageToast",
  ],
  function (BaseController, JSONModel, formatter, mobileLibrary, MessageToast) {
    "use strict";

    // shortcut for sap.m.URLHelper
    var URLHelper = mobileLibrary.URLHelper;
    var oVizFrame = null;

    return BaseController.extend("com.stulz.zprapvbud.controller.Detail", {
      formatter: formatter,

      /* =========================================================== */
      /* lifecycle methods                                           */
      /* =========================================================== */

      onInit: function () {
        // Model used to manipulate control states. The chosen values make sure,
        // detail page is busy indication immediately so there is no break in
        // between the busy indication for loading the view's meta data
        var oViewModel = new JSONModel({
          busy: false,
          delay: 0,
          lineItemListTitle: this.getResourceBundle().getText(
            "detailLineItemTableHeading"
          ),
        });

        this.getRouter()
          .getRoute("object")
          .attachPatternMatched(this._onObjectMatched, this);

        this.setModel(oViewModel, "detailView");

        this.getOwnerComponent()
          .getModel()
          .metadataLoaded()
          .then(this._onMetadataLoaded.bind(this));

        oVizFrame = this.getView().byId("idpiechart");

        var new_2 = {
          Sales: [],
        };
        var oModel2 = new sap.ui.model.json.JSONModel(new_2);
        this.byId("idpiechart").setModel(oModel2);

        var oDataset = new sap.viz.ui5.data.FlattenedDataset({
          dimensions: [
            {
              name: "BudgetUtil",
              value: "{BudgetUtil}",
            },
          ],

          measures: [
            {
              name: "BudgetRes",
              value: "{BudgetRes}",
            },
          ],

          data: {
            path: "/Sales",
          },
        });
        oVizFrame.setDataset(oDataset);

        //      4.Set Viz properties
        oVizFrame.setVizProperties({
          title: {
            text: "BUDGET",
          },
          plotArea: {
            colorPalette: d3.scale.category20().range(),
            drawingEffect: "glossy",
          },
        });

        var feedSize = new sap.viz.ui5.controls.common.feeds.FeedItem({
            uid: "size",
            type: "Measure",
            values: ["BudgetRes"],
          }),
          feedColor = new sap.viz.ui5.controls.common.feeds.FeedItem({
            uid: "color",
            type: "Dimension",
            values: ["BudgetUtil"],
          });
        oVizFrame.addFeed(feedSize);
        oVizFrame.addFeed(feedColor);

        var donutProp = {
          plotArea: {
            dataLabel: {
              visible: true,
            },
          },
          legend: {
            title: {
              visible: false,
            },
          },
          legendGroup: {
            layout: {
              position: "bottom",
            },
          },
          title: { text: "", visible: false },
          valueAxis: {
            title: {
              visible: false,
            },
            label: {
              formatString:
                sap.viz.ui5.format.ChartFormatter.DefaultPattern.LONGFLOAT,
            },
          },
          categoryAxis: {
            title: {
              visible: false,
            },
          },
        };

        oVizFrame.setVizProperties(donutProp);
      },

      /* =========================================================== */
      /* event handlers                                              */
      /* =========================================================== */

      onTabContainerItemClose: function (event) {
        event.preventDefault();
      },
      /**
       * Event handler when the share by E-Mail button has been clicked
       * @public
       */
      onSendEmailPress: function () {
        var oViewModel = this.getModel("detailView");

        URLHelper.triggerEmail(
          null,
          oViewModel.getProperty("/shareSendEmailSubject"),
          oViewModel.getProperty("/shareSendEmailMessage")
        );
      },

      /**
       * Updates the item count within the line item table's header
       * @param {object} oEvent an event containing the total number of items in the list
       * @private
       */
      onListUpdateFinished: function (oEvent) {
        var sTitle,
          iTotalItems = oEvent.getParameter("total"),
          oViewModel = this.getModel("detailView");

        // only update the counter if the length is final
        if (this.byId("lineItemsList").getBinding("items").isLengthFinal()) {
          if (iTotalItems) {
            sTitle = this.getResourceBundle().getText(
              "detailLineItemTableHeadingCount",
              [iTotalItems]
            );
          } else {
            //Display 'Line Items' instead of 'Line items (0)'
            sTitle = this.getResourceBundle().getText(
              "detailLineItemTableHeading"
            );
          }
          oViewModel.setProperty("/lineItemListTitle", sTitle);
        }
      },

      /* =========================================================== */
      /* begin: internal methods                                     */
      /* =========================================================== */

      /**
       * Binds the view to the object path and expands the aggregated line items.
       * @function
       * @param {sap.ui.base.Event} oEvent pattern match event in route 'object'
       * @private
       */
      _onObjectMatched: function (oEvent) {
        console.log("_onObjectMatched");
        var Banfn = oEvent.getParameter("arguments").Banfn;
        var Bnfpo = oEvent.getParameter("arguments").Bnfpo;


        this.getModel("appView").setProperty(
          "/layout",
          "TwoColumnsMidExpanded"
        );
        this.getModel()
          .metadataLoaded()
          .then(
            function () {
              var sObjectPath = this.getModel().createKey("RdaSet", {
                Banfn: Banfn,
                Bnfpo: Bnfpo,
              });
              console.log(sObjectPath);
              this._bindView("/" + sObjectPath);
            }.bind(this)
          );
      },

      /**
       * Binds the view to the object path. Makes sure that detail view displays
       * a busy indicator while data for the corresponding element binding is loaded.
       * @function
       * @param {string} sObjectPath path to the object to be bound to the view.
       * @private
       */
      _bindView: function (sObjectPath) {
        // Set busy indicator during view binding
        var oViewModel = this.getModel("detailView");

        // If the view was not bound yet its not busy, only if the binding requests data it is set to busy again
        oViewModel.setProperty("/busy", false);

        this.getView().bindElement({
          path: sObjectPath,
          events: {
            change: this._onBindingChange.bind(this),
            dataRequested: function () {
              oViewModel.setProperty("/busy", true);
            },
            dataReceived: function () {
              oViewModel.setProperty("/busy", false);
            },
          },
        });
        
            
      },
      formatterIcon: function (value,flg) {
          console.log(flg);
          if(flg == "X" ){
           return "sap-icon://status-error";   
          }else{
            switch (value) {
            case "1":
                return "sap-icon://accept";
                break;
            case "2":
                return "sap-icon://cancel";
                break;
            case "3":
                return "sap-icon://warning";
                break;

            default:
                return "sap-icon://email";
                break;
            }
          }
      },

      _onBindingChange: function () {
        var oView = this.getView(),
          that = this,
          oElementBinding = oView.getElementBinding();

        // No data for the binding
        if (!oElementBinding.getBoundContext()) {
          this.getRouter().getTargets().display("detailObjectNotFound");
          // if object could not be found, the selection in the master list
          // does not make sense anymore.
          this.getOwnerComponent().oListSelector.clearMasterListSelection();
          return;
        } else {
          var sPath = oElementBinding.getPath(),
            oResourceBundle = this.getResourceBundle(),
            oObject = oView.getModel().getObject(sPath),
            sObjectId = oObject.Banfn,
            sObjectName = oObject.Bnfpo,
            oViewModel = this.getModel("detailView");
            this.setDetail(oObject);
          var oModelData1 = this.getOwnerComponent().getModel();

          oModelData1.read(
            "/RdaSet(Banfn='" +
              sObjectId +
              "',Bnfpo='" +
              sObjectName +
              "')/Budget",
            {
              success: function (oObject) {
                console.log(oObject);

                var oModel = new sap.ui.model.json.JSONModel();
                var data = {
                  Sales: [
                    {
                      BudgetUtil: "Budget Utililzzato",
                      BudgetRes: oObject.BudgetUtil,
                    },
                    {
                      BudgetUtil: "Budget Residuo",
                      BudgetRes: oObject.BudgetRes,
                    },
                  ],
                };
                oModel.setData(data);
                oVizFrame.setModel(oModel);
              },
              error: function (oError) {
                console.log(oError);
              },
            }
          );

          oModelData1.read(
            "/RdaSet(Banfn='" +
              sObjectId +
              "',Bnfpo='" +
              sObjectName +
              "')/DocumentoSet",
            {
              success: function (oObjectRes) {
                console.log(oObjectRes.results);
                var oViewModel = that.getModel("detailView");
                oViewModel.setProperty("/numFile", oObjectRes.results.length);
              },
              error: function (oError) {
                console.log(oError);
              },
            }
          );
        }
      },

      _onMetadataLoaded: function () {
        // Store original busy indicator delay for the detail view
        var iOriginalViewBusyDelay = this.getView().getBusyIndicatorDelay(),
          oViewModel = this.getModel("detailView");

        // Make sure busy indicator is displayed immediately when
        // detail view is displayed for the first time
        oViewModel.setProperty("/delay", 0);

        // Binding the view will set it to not busy - so the view is always busy if it is not bound
        oViewModel.setProperty("/busy", true);
        // Restore original busy indicator delay for the detail view
        oViewModel.setProperty("/delay", iOriginalViewBusyDelay);
      },

      /**
       * Set the full screen mode to false and navigate to master page
       */
      onCloseDetailPress: function () {
        this.getModel("appView").setProperty(
          "/actionButtonsInfo/midColumn/fullScreen",
          false
        );
        // No item should be selected on master after detail page is closed
        this.getOwnerComponent().oListSelector.clearMasterListSelection();
        this.getRouter().navTo("master");
        this.setDetail(null);
      },

      /**
       * Toggle between full and non full screen mode.
       */
      toggleFullScreen: function () {
        var bFullScreen = this.getModel("appView").getProperty(
          "/actionButtonsInfo/midColumn/fullScreen"
        );
        this.getModel("appView").setProperty(
          "/actionButtonsInfo/midColumn/fullScreen",
          !bFullScreen
        );
        if (!bFullScreen) {
          // store current layout and go full screen
          this.getModel("appView").setProperty(
            "/previousLayout",
            this.getModel("appView").getProperty("/layout")
          );
          this.getModel("appView").setProperty(
            "/layout",
            "MidColumnFullScreen"
          );
        } else {
          // reset to previous layout
          this.getModel("appView").setProperty(
            "/layout",
            this.getModel("appView").getProperty("/previousLayout")
          );
        }
      },
      _getDialog: function () {
        if (!this._oDialog) {
          this._oDialog = sap.ui.xmlfragment(
            "idFragment",
            "com.stulz.zprapvbud.view.Dialog",
            this
          );
          this.getView().addDependent(this._oDialog);
        }
        return this._oDialog;
      },
      openDialog: function () {
        var bindingContext = this.getView().getBindingContext();
        var path = bindingContext.getPath();
        var object = bindingContext.getModel().getProperty(path);
        this._getDialog().open();
      },
      /*
      _getDialogApprove: function () {
        if (!this._oDialog) {
          this._oDialog = sap.ui.xmlfragment(
            "idFragment",
            "com.stulz.zprapvbud.view.DialogApprove",
            this
          );
          this.getView().addDependent(this._oDialog);
        }
        return this._oDialog;
      },
      _getDialogReject: function () {
        if (!this._oDialogReject) {
          this._oDialogReject = sap.ui.xmlfragment(
            "idFragment",
            "com.stulz.zprapvbud.view.DialogReject",
            this
          );
          this.getView().addDependent(this._oDialogReject);
        }
        return this._oDialogReject;
      },
      _getDialogBudget: function () {
        if (!this._oDialogBudget) {
          this._oDialogBudget = sap.ui.xmlfragment(
            "idFragment",
            "com.stulz.zprapvbud.view.DialogBudget",
            this
          );
          this.getView().addDependent(this._oDialogBudget);
        }
        return this._oDialogBudget;
      },

      openDialogApprove: function () {
        var bindingContext = this.getView().getBindingContext();
        var path = bindingContext.getPath();
        var object = bindingContext.getModel().getProperty(path);
        this._getDialogApprove().open();
      },

            openDialogBudget: function () {
        var bindingContext = this.getView().getBindingContext();
        var path = bindingContext.getPath();
        var object = bindingContext.getModel().getProperty(path);
        this._getDialogBudget().open();

        var valoreIncremento = 0;
        console.log(parseFloat(object.BudgetRes));
        if (parseFloat(object.BudgetRes) < 0) {
          valoreIncremento = parseFloat(object.BudgetRes) * -1;
          console.log(valoreIncremento);
          sap.ui
            .getCore()
            .byId("idFragment--inputNumber")
            .setValue(valoreIncremento);
        } else {
          sap.ui.getCore().byId("idFragment--inputNumber").setValue(0);
        }
      },
      closeDialogReject: function () {
        this._oDialogReject.close();
      },

            approva: function () {
        var that = this;

        var oView = this.getView(),
          oElementBinding = oView.getElementBinding();

        var sPath = oElementBinding.getPath(),
          oResourceBundle = this.getResourceBundle(),
          oObject = oView.getModel().getObject(sPath),
          Banfn = oObject.Banfn,
          Bnfpo = oObject.Bnfpo;

        var bindingContext = this.getView().getBindingContext();
        var path = bindingContext.getPath();
        var object = {};

        object.Banfn = Banfn;
        object.Bnfpo = Bnfpo;
        object.TestoApv = sap.ui
          .getCore()
          .byId("idFragment--richiesta_nota")
          .getValue();
        object.StatoApv = "1";
        console.log(object);
        var oModelData1 = this.getOwnerComponent().getModel();
        oModelData1.update(path, object, {
          success: function (oRetrievedResult) {
            MessageToast.show("Salvata");
            sap.ui.getCore().byId("idFragment--richiesta_nota").setValue("");

            that.closeDialog();
            //refresh di tutti i componenti
            that.getOwnerComponent().getModel().refresh();
            that.getRouter().navTo("master");
          },
          error: function (oError) {
            console.log(oError);
            that.closeDialog();
          },
        });
      },

      rifiuta: function () {
        var that = this;

        var oView = this.getView(),
          oElementBinding = oView.getElementBinding();

        var sPath = oElementBinding.getPath(),
          oResourceBundle = this.getResourceBundle(),
          oObject = oView.getModel().getObject(sPath),
          Banfn = oObject.Banfn,
          Bnfpo = oObject.Bnfpo;

        var bindingContext = this.getView().getBindingContext();
        var path = bindingContext.getPath();
        var object = {};

        object.Banfn = Banfn;
        object.Bnfpo = Bnfpo;
        object.TestoApv = sap.ui
          .getCore()
          .byId("idFragment--richiesta_nota_reject")
          .getValue();
        object.StatoApv = "2";
        console.log(object);
        if (
          sap.ui
            .getCore()
            .byId("idFragment--richiesta_nota_reject")
            .getValue() != ""
        ) {
          var oModelData1 = this.getOwnerComponent().getModel();
          oModelData1.update(path, object, {
            success: function (oRetrievedResult) {
              MessageToast.show("Salvata");
              sap.ui
                .getCore()
                .byId("idFragment--richiesta_nota_reject")
                .setValue("");

              that.closeDialogReject();
              //refresh di tutti i componenti

              that.getOwnerComponent().getModel().refresh();
              that.getRouter().navTo("master");
            },
            error: function (oError) {
              console.log(oError);
              that.closeDialogReject();
            },
          });
        } else {
          MessageToast.show("Inserire il motivo rifiuto ");
        }
      },
        updateBudget: function () {
        var that = this;

        var bindingContext = this.getView().getBindingContext();
        var path = bindingContext.getPath();
        var object = bindingContext.getModel().getProperty(path);

        console.log(path);
        var value = sap.ui.getCore().byId("idFragment--inputNumber").getValue();
        if (value > 0) {
          object.Wert = value;
          var oModelData1 = this.getOwnerComponent().getModel();

          var new_obj = {
            Wert: object.Wert,
            Banfn: object.Banfn,
            Bnfpo: object.Bnfpo,
            Waers: object.Waers,
          };
          console.log("new pbject");

          console.log(new_obj);
          oModelData1.update(
            "/BudgetSet(Banfn='" +
              object.Banfn +
              "',Bnfpo='" +
              object.Bnfpo +
              "')",
            new_obj,
            {
              success: function (oRetrievedResult) {
                MessageToast.show("Aggiorato");
                sap.ui.getCore().byId("idFragment--inputNumber").setValue("");

                that.closeDialogBudget();
                //refresh di tutti i componenti
                that.getOwnerComponent().getModel().refresh();
              },
              error: function (oError) {
                that.closeDialogBudget();
              },
            }
          );
        } else {
          MessageToast.show("Errore");
        }
      },
           closeDialogBudget: function () {
        this._oDialogBudget.close();
      },
*/

      closeDialog: function () {
        this._oDialog.close();
      },
      saveNota: function () {
        var that = this;
        var oView = this.getView(),
          oElementBinding = oView.getElementBinding(),
          sPath = oElementBinding.getPath(),
          oObject = oView.getModel().getObject(sPath),
          sObjectId = oObject.Banfn,
          sObjectName = oObject.Bnfpo;
        var bindingContext = this.getView().getBindingContext();
        var path = bindingContext.getPath();
        var object = {
          Banfn: sObjectId,
          Bnfpo: oObject.Bnfpo,
          Nota: sap.ui.getCore().byId("idFragment--nota").getValue(),
        };
        console.log(object);
        var oModelData1 = this.getOwnerComponent().getModel();
        oModelData1.create("/CronologiaSet", object, {
          success: function (oRetrievedResult) {
            MessageToast.show("Salvata");
            sap.ui.getCore().byId("idFragment--nota").setValue("");

            that.closeDialog();
            //refresh di tutti i componenti
            that.getOwnerComponent().getModel().refresh();
          },
          error: function (oError) {
            console.log(oError);
            that.closeDialog();
          },
        });
      },

      onDownloadItemSmart: function (event) {
        console.log("onDownloadItemSmart");
        var that = this;
        var that = this;
        var Ext = event.getSource().getBindingContext().getObject().Ext;

        console.log(event.getSource().getBindingContext().getObject());
        var Banfn = event.getSource().getBindingContext().getObject().Banfn;
        var Bnfpo = event.getSource().getBindingContext().getObject().Bnfpo;
        var ArcDocId = event.getSource().getBindingContext().getObject()
          .ArcDocId;

        var oModelData1 = this.getOwnerComponent().getModel();

        oModelData1.read(
          "/DownloadSet(Banfn='" +
            Banfn +
            "',Bnfpo='" +
            Bnfpo +
            "',ArcDocId='" +
            ArcDocId +
            "')",
          {
            success: function (oRetrievedResult) {
              console.log(oRetrievedResult.Datadoc);

              var contentType = "";

              switch (Ext) {
                case "DOCX":
                  contentType =
                    "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                  break;
                case "DOC":
                  contentType = "application/msword";
                  break;
                case "XLSX":
                  contentType =
                    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                  break;
                case "XLS":
                  contentType = "application/vnd.ms-excel";
                  break;
                case "PPT":
                  contentType = "application/vnd.ms-powerpoint";
                  break;
                case "PPTX":
                  contentType =
                    "application/vnd.openxmlformats-officedocument.presentationml.presentation";
                  break;
                case "PNG":
                  contentType = "image/png";
                  break;
                case "JPEG":
                  contentType = "image/png";
                  break;
                case "JPG":
                  contentType = "image/png";
                  break;
                case "SVG":
                  contentType = "image/svg+xml";
                  break;
                case "PDF":
                  contentType = "application/pdf";
                  break;
                case "MSG":
                  contentType = "octet/stream";
                  break;

                  default:
                  contentType = "application/*";
                  break;
              }

              var a =
                "data:" +
                contentType +
                ";base64," +
                oRetrievedResult.Datadoc +
                "";
              var o = document.createElement("a");
              var i = oRetrievedResult.ArcDocId;
              o.href = a;
              if (Ext != "MSG") {
                o.download = i;
              } else {
                o.download = i + ".msg";
              }

              o.click();
            },
            error: function (oError) {
              console.log(oError);
            },
          }
        );
      },
      onDownloadItemSmart2: function (event) {
        console.log("onDownloadItemSmart");
        var that = this;
        var that = this;
        var Ext = event.getSource().getBindingContext().getObject().Ext;

        console.log(event.getSource().getBindingContext().getObject());
        var Banfn = event.getSource().getBindingContext().getObject().Banfn;
        var Bnfpo = event.getSource().getBindingContext().getObject().Bnfpo;
        var ArcDocId = event.getSource().getBindingContext().getObject()
          .ArcDocId;

        var oModelData1 = this.getOwnerComponent().getModel();

        oModelData1.read(
          "/DownloadSet(Banfn='" +
            Banfn +
            "',Bnfpo='" +
            Bnfpo +
            "',ArcDocId='" +
            ArcDocId +
            "')",
          {
            success: function (oRetrievedResult) {
              console.log(oRetrievedResult.Datadoc);

              var contentType = "";

              switch (Ext) {
                case "DOCX":
                  contentType =
                    "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                  break;
                case "DOC":
                  contentType = "application/msword";
                  break;
                case "XLSX":
                  contentType =
                    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                  break;
                case "XLS":
                  contentType = "application/vnd.ms-excel";
                  break;
                case "PPT":
                  contentType = "application/vnd.ms-powerpoint";
                  break;
                case "PPTX":
                  contentType =
                    "application/vnd.openxmlformats-officedocument.presentationml.presentation";
                  break;
                case "PNG":
                  contentType = "image/png";
                  break;
                case "JPEG":
                  contentType = "image/png";
                  break;
                case "JPG":
                  contentType = "image/png";
                  break;
                case "SVG":
                  contentType = "image/svg+xml";
                  break;
                case "PDF":
                  contentType = "application/pdf";
                  break;
                case "MSG":
                  contentType = "octet/stream";
                  break;

                default:
                  contentType = "application/*";
                  break;
              }
              if (Ext != "MSG") {

              var b64Data = oRetrievedResult.Datadoc;

              var blob = that.b64toBlob(b64Data, contentType);
              var blobUrl = URL.createObjectURL(blob);
              window.open(blobUrl);
              }else{
                "data:" +
                  contentType +
                  ";base64," +
                  oRetrievedResult.Datadoc +
                  "";
                var o = document.createElement("a");
                var i = oRetrievedResult.ArcDocId;
                o.href = a;
                o.download = i + ".msg";
                o.click();


              }
            },
            error: function (oError) {
              console.log(oError);
            },
          }
        );
      },
      b64toBlob: function (b64Data, contentType) {
        var sliceSize = 512;
        const byteCharacters = atob(b64Data);
        const byteArrays = [];

        for (
          let offset = 0;
          offset < byteCharacters.length;
          offset += sliceSize
        ) {
          const slice = byteCharacters.slice(offset, offset + sliceSize);

          const byteNumbers = new Array(slice.length);
          for (let i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
          }

          const byteArray = new Uint8Array(byteNumbers);
          byteArrays.push(byteArray);
        }

        const blob = new Blob(byteArrays, { type: contentType });
        return blob;
      },
      formatterDate: function (date, time) {
        // SAPUI5 formatters
        var dateFormat = sap.ui.core.format.DateFormat.getDateInstance({
          pattern: "dd/MM/yyyy",
        });
        var timeFormat = sap.ui.core.format.DateFormat.getTimeInstance({
          pattern: "KK:mm:ss a",
        });
        // timezoneOffset is in hours convert to milliseconds
        var TZOffsetMs = new Date(0).getTimezoneOffset() * 60 * 1000;
        // format date and time to strings offsetting to GMT
        var dateStr = dateFormat.format(new Date(date.getTime() + TZOffsetMs)); //05-12-2012
        var timeStr = timeFormat.format(new Date(time.ms + TZOffsetMs)); //11:00 AM
        //parse back the strings into date object back to Time Zone
        var parsedDate = new Date(
          dateFormat.parse(dateStr).getTime() - TZOffsetMs
        ); //1354665600000
        var parsedTime = new Date(
          timeFormat.parse(timeStr).getTime() - TZOffsetMs
        ); //39600000

        console.log(dateStr + timeStr);
        return dateStr + "  " + timeStr;
      },
    });
  }
);
