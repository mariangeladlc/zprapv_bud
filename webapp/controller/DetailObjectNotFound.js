sap.ui.define([
	"./BaseController"
], function (BaseController) {
	"use strict";

	return BaseController.extend("com.stulz.zprapvbud.controller.DetailObjectNotFound", {});
});
