sap.ui.define(
  ["./BaseController", "sap/ui/model/json/JSONModel", "sap/m/MessageToast"],
  function (BaseController, JSONModel, MessageToast) {
    "use strict";

    return BaseController.extend("com.stulz.zprapvbud.controller.App", {
      onInit: function () {
        var oViewModel,
          fnSetAppNotBusy,
          iOriginalBusyDelay = this.getView().getBusyIndicatorDelay();

        oViewModel = new JSONModel({
          busy: true,
          delay: 0,
          layout: "OneColumn",
          previousLayout: "",
          actionButtonsInfo: {
            midColumn: {
              fullScreen: false,
            },
          },
        });
        this.setModel(oViewModel, "appView");

        fnSetAppNotBusy = function () {
          oViewModel.setProperty("/busy", false);
          oViewModel.setProperty("/delay", iOriginalBusyDelay);
        };

        // since then() has no "reject"-path attach to the MetadataFailed-Event to disable the busy indicator in case of an error
        this.getOwnerComponent()
          .getModel()
          .metadataLoaded()
          .then(fnSetAppNotBusy);
        this.getOwnerComponent()
          .getModel()
          .attachMetadataFailed(fnSetAppNotBusy);

        // apply content density mode to root view
        this.getView().addStyleClass(
          this.getOwnerComponent().getContentDensityClass()
        );
      },
      openDialogApprove: function () {
        var text = "";
        if (this.getMode() == "None") {
          if (this.getDetail() != null) {
            text =
              text +
              "\u000a" +
              " - " +
              this.getDetail().Banfn +
              " " +
              this.getDetail().Bnfpo;
            this._getDialogApprove().open();
            sap.ui.getCore().byId("idFragment--approvedList").setText(text);
          } else {
            MessageToast.show("Nessuna richiesta di acquisto selezionata.");
          }
        } else {
          var oList = this.getList();
          var oBinding = oList.getBinding("items");
          var items = oList.getSelectedItems();
          if (items.length > 0) {
            for (var i = 0; i < items.length; i++) {
              var item = items[i];
              var context = item.getBindingContext();
              var obj = context.getProperty(null, context);
              text = text + "\u000a" + " - " + obj.Banfn + " " + obj.Bnfpo;
            }
            this._getDialogApprove().open();
            sap.ui.getCore().byId("idFragment--approvedList").setText(text);
          } else {
            MessageToast.show("Nessuna richiesta di acquisto selezionata.");
          }
        }
      },
      openDialogBudget: function () {
        var text = "";
        var valoreIncremento = 0;

        if (this.getMode() == "None") {
          if (this.getDetail() != null) {
            console.log(this.getDetail());
            text =
              text +
              "\u000a" +
              " - " +
              this.getDetail().Banfn +
              " " +
              this.getDetail().Bnfpo;
            this._getDialogBudget().open();
            sap.ui
              .getCore()
              .byId("idFragment--approvedListBudget")
              .setText(text);
            sap.ui
              .getCore()
              .byId("idFragment--inputNumber")
              .setValue(valoreIncremento);
          } else {
            MessageToast.show("Nessuna richiesta di acquisto selezionata.");
          }
        } else {
          var oList = this.getList();
          var oBinding = oList.getBinding("items");
          var items = oList.getSelectedItems();
          if (items.length > 0) {
            for (var i = 0; i < items.length; i++) {
              var item = items[i];
              var context = item.getBindingContext();
              var obj = context.getProperty(null, context);
              text = text + "\u000a" + " - " + obj.Banfn + " " + obj.Bnfpo;
            }
            if (items.length == 1) {
              var object = items[0];
              console.log(parseFloat(object.BudgetRes));
              if (parseFloat(object.BudgetRes) < 0) {
                valoreIncremento = parseFloat(object.BudgetRes) * -1;
                console.log(valoreIncremento);
              }
            }
            this._getDialogBudget().open();
            sap.ui
              .getCore()
              .byId("idFragment--approvedListBudget")
              .setText(text);
            sap.ui
              .getCore()
              .byId("idFragment--inputNumber")
              .setValue(valoreIncremento);
          } else {
            MessageToast.show("Nessuna richiesta di acquisto selezionata.");
          }
        }
      },

      openDialogReject: function () {
        var text = "";
        console.log(this.getMode());
        if (this.getMode() == "None") {
          if (this.getDetail() != null) {
            text =
              text +
              "\u000a" +
              " - " +
              this.getDetail().Banfn +
              " " +
              this.getDetail().Bnfpo;
            this._getDialogReject().open();
            sap.ui.getCore().byId("idFragment--rejectList").setText(text);
          } else {
            MessageToast.show("Nessuna richiesta di acquisto selezionata.");
          }
        } else {
          var oList = this.getList();
          var oBinding = oList.getBinding("items");
          var items = oList.getSelectedItems();
          if (items.length > 0) {
            for (var i = 0; i < items.length; i++) {
              var item = items[i];
              var context = item.getBindingContext();
              var obj = context.getProperty(null, context);
              text = text + "\u000a" + " - " + obj.Banfn + " " + obj.Bnfpo;
            }
            this._getDialogReject().open();
            sap.ui.getCore().byId("idFragment--rejectList").setText(text);
          } else {
            MessageToast.show("Nessuna richiesta di acquisto selezionata.");
          }
        }
      },
      closeDialogReject: function () {
        this._oDialogReject.close();
      },
      approvaModeNone: function () {
        console.log(this.getDetail());
        var list = [];
        var object = {};
        object.Banfn = this.getDetail().Banfn;
        object.Bnfpo = this.getDetail().Bnfpo;
        object.TestoApv = sap.ui
          .getCore()
          .byId("idFragment--richiesta_nota")
          .getValue();
        object.StatoApv = "1";
        list.push(object);

        var header = {
          Id: "1",
          RdaSet: list,
        };
        this.approvaSingle(header);
      },
      approvaModeMulti: function () {
        var that = this;
        var oList = this.getList();
        var oBinding = oList.getBinding("items");
        var items = oList.getSelectedItems();
        var list = [];
        if (items.length > 0) {
          for (var i = 0; i < items.length; i++) {
            var item = items[i];
            var context = item.getBindingContext();
            var obj = context.getProperty(null, context);
            var object = {};
            object.Banfn = obj.Banfn;
            object.Bnfpo = obj.Bnfpo;
            object.TestoApv = sap.ui
              .getCore()
              .byId("idFragment--richiesta_nota")
              .getValue();
            object.StatoApv = "1";
            list.push(object);
          }
          var header = {
            Id: "1",
            RdaSet: list,
          };
          this.approvaSingle(header);
        } else {
          MessageToast.show(
            "Nessun elemento selezionato. \u000a Spunta un elemento dalla lista"
          );
        }
      },

      approva: function () {
        if (this.getMode() == "None") {
          this.approvaModeNone();
        } else {
          this.approvaModeMulti();
        }
      },
      rifiuta: function () {
        console.log(this.getMode());
        if (this.getMode() == "None") {
          this.rifiutaModeNone();
        } else {
          this.rifiutaModeMulti();
        }
      },
      budgetModeNone() {
        console.log(this.getDetail());
        if (this.getDetail != null) {
                var list = [];
                    var object = this.getDetail();
                    console.log(object);
                    var value = sap.ui
                    .getCore()
                    .byId("idFragment--inputNumber")
                    .getValue();
                    if (value > 0) {
                    object.Wert = value;
                    var oModelData1 = this.getOwnerComponent().getModel();

                    var new_obj = {
                        Wert: object.Wert,
                        Banfn: object.Banfn,
                        Bnfpo: object.Bnfpo,
                        Waers: object.Waers,
                    };
                    list.push(new_obj);
                    }
                

                var header = {
                    Id: "1",
                    BudgetSet: list,
                };
                this.budgetSingle(header);
                

        } else {
          MessageToast.show("Nessuna richiesta di acquisto selezionata");
        }
      },
      budgetSingle: function (object) {
        var that = this;
        var oModelData1 = this.getOwnerComponent().getModel();
        console.log(object);
        oModelData1.create("/ContainerBudgetSet", object, {
          success: function (oRetrievedResult) {
            MessageToast.show("Aggiorato");
            sap.ui.getCore().byId("idFragment--inputNumber").setValue("");

            that.closeDialogBudget();
            //refresh di tutti i componenti
            that.getOwnerComponent().getModel().refresh();
          },
          error: function (oError) {
            that.closeDialogBudget();
          },
        });
      },
      budgetModeMulti: function () {
        var that = this;
        var text = "";
        var oList = this.getList();
        var oBinding = oList.getBinding("items");
        var items = oList.getSelectedItems();
        console.log(items);
        var list = [];
        if (items.length > 0) {
          for (var i = 0; i < items.length; i++) {
            var item = items[i];
            var context = item.getBindingContext();
            var object = context.getProperty(null, context);
            console.log(object);
            var value = sap.ui
              .getCore()
              .byId("idFragment--inputNumber")
              .getValue();
            if (value > 0) {
              object.Wert = value;
              var oModelData1 = this.getOwnerComponent().getModel();

              var new_obj = {
                Wert: object.Wert,
                Banfn: object.Banfn,
                Bnfpo: object.Bnfpo,
                Waers: object.Waers,
              };
              list.push(new_obj);
            }
          }

          var header = {
            Id: "1",
            BudgetSet: list,
          };
          this.budgetSingle(header);
        }
      },
      updateBudget: function () {
        if (this.getMode() == "None") {
          this.budgetModeNone();
        } else {
          this.budgetModeMulti();
        }
      },
      approvaSingle: function (object) {
        var that = this;
        var oModelData1 = this.getOwnerComponent().getModel();
        oModelData1.create("/ContainerSet", object, {
          success: function (oRetrievedResult) {
            MessageToast.show("Salvata");
            sap.ui.getCore().byId("idFragment--richiesta_nota").setValue("");
            that.closeDialog();
            that.getOwnerComponent().getModel().refresh();
            that.getRouter().navTo("master");
          },
          error: function (oError) {
            console.log(oError);
            that.closeDialog();
          },
        });
      },

      rifiutaModeMulti() {
        var that = this;
        var oList = this.getList();
        var oBinding = oList.getBinding("items");
        var items = oList.getSelectedItems();
        var list = [];
        if (items.length > 0) {
          for (var i = 0; i < items.length; i++) {
            var item = items[i];
            var context = item.getBindingContext();
            var obj = context.getProperty(null, context);
            var object = {};
            object.Banfn = obj.Banfn;
            object.Bnfpo = obj.Bnfpo;
            object.TestoApv = sap.ui
              .getCore()
              .byId("idFragment--richiesta_nota_reject")
              .getValue();
            object.StatoApv = "2";
            list.push(object);
          }
          var header = {
            Id: "1",
            RdaSet: list,
          };
          this.rifiutaSingle(header);
        } else {
          MessageToast.show(
            "Nessun elemento selezionato. \u000a Spunta un elemento dalla lista"
          );
        }
      },
      rifiutaModeNone() {
        console.log(this.getDetail());
        var list = [];
        var object = {};
        object.Banfn = this.getDetail().Banfn;
        object.Bnfpo = this.getDetail().Bnfpo;
        object.TestoApv = sap.ui
          .getCore()
          .byId("idFragment--richiesta_nota_reject")
          .getValue();
        object.StatoApv = "2";

        list.push(object);

        var header = {
          Id: "1",
          RdaSet: list,
        };
        this.rifiutaSingle(header);
      },

      rifiutaSingle: function (object) {
        var that = this;
        console.log(object);
        if (
          sap.ui
            .getCore()
            .byId("idFragment--richiesta_nota_reject")
            .getValue() != ""
        ) {
          var oModelData1 = this.getOwnerComponent().getModel();
          oModelData1.create("/ContainerSet", object, {
            success: function (oRetrievedResult) {
              MessageToast.show("Salvata");
              sap.ui
                .getCore()
                .byId("idFragment--richiesta_nota_reject")
                .setValue("");

              that.closeDialogReject();
              //refresh di tutti i componenti

              that.getOwnerComponent().getModel().refresh();
              that.getRouter().navTo("master");
            },
            error: function (oError) {
              console.log(oError);
              that.closeDialogReject();
            },
          });
        } else {
          MessageToast.show("Inserire il motivo rifiuto ");
        }
      },
      _getDialogApprove: function () {
        if (!this._oDialog) {
          this._oDialog = sap.ui.xmlfragment(
            "idFragment",
            "com.stulz.zprapvbud.view.DialogApprove",
            this
          );
          this.getView().addDependent(this._oDialog);
        }
        return this._oDialog;
      },
      _getDialogReject: function () {
        if (!this._oDialogReject) {
          this._oDialogReject = sap.ui.xmlfragment(
            "idFragment",
            "com.stulz.zprapvbud.view.DialogReject",
            this
          );
          this.getView().addDependent(this._oDialogReject);
        }
        return this._oDialogReject;
      },
      closeDialog: function () {
        this._oDialog.close();
      },
      _getDialogBudget: function () {
        if (!this._oDialogBudget) {
          this._oDialogBudget = sap.ui.xmlfragment(
            "idFragment",
            "com.stulz.zprapvbud.view.DialogBudget",
            this
          );
          this.getView().addDependent(this._oDialogBudget);
        }
        return this._oDialogBudget;
      },
      _getDialogBudget: function () {
        if (!this._oDialogBudget) {
          this._oDialogBudget = sap.ui.xmlfragment(
            "idFragment",
            "com.stulz.zprapvbud.view.DialogBudget",
            this
          );
          this.getView().addDependent(this._oDialogBudget);
        }
        return this._oDialogBudget;
      },

      closeDialogBudget: function () {
        this._oDialogBudget.close();
      },
      returnLaunchpad: function () {
        window.location.replace(window.location.pathname);
      },
    });
  }
);
